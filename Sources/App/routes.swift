import Vapor
import Foundation

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
    router.get { req in
        return "It works!"
    }

///---------------------------------------------------------
/// URI Parameters
///---------------------------------------------------------
    // Basic "Hello, world!" example
    router.get("hello") { req in
        return "Hello, world!"
    }
    
    // create another route
    router.get("hello", "vapor") { _ in
        return "Hello, Vapor!"
    }
    /*
     Add a new route to handle a GET request. Each parameter to router.get is a path component in the URL. This route is invoked when a user enters http://localhost:8080/hello/vapor as the URL.
     Supply a closure to run when this route is invoked. The closure receives a Request object.
     Return a string as the result for this route.
     */
    
    
    // Add a new route that says hello to whomever visits.
    /* For example, if your name is Tim, you’ll visit the app using the URL http://localhost:8080/hello/Tim and it says “Hello, Tim!” */
    router.get("hello", String.parameter) { req -> String in
        let name = try req.parameters.next(String.self)
        return "Hello, \(name)!"
    }
    /*
     Use String.parameter to specify that the second parameter can be any String.
     Extract the user’s name, which is passed in the Request object.
     Use the name to return your greeting.
     */
    
    
///---------------------------------------------------------
/// Accepting Data using Query Parameter (JSON body)
// <base URL>/info
///---------------------------------------------------------
    // a client sends a POST request with a JSON body which the app must decode and process.
    // You give Vapor a Codable struct that matches your expected data and Vapor does the rest.
    // ! use a REST client to test your APIs
    router.post(InfoData.self, at: "info") { req, data -> String in
        return "Hello, \(data.name)! :D"
    }
    /*
     Adds a new route handler to handle a POST request for the URL http://localhost:8080/info.
     This route handler returns a String.
     The route handler accepts a Content type as the first parameter and any path parameters after the at: parameter name.
     The route handler decodes the data and passes it to the closure as the second parameter.
     Returns the string by pulling the name out of the data variable.
     */
    

///---------------------------------------------------------
/// Accepting Data using Query Parameter (JSON body)
/// AND Returning JSON
// <base URL>/inforesponse
///---------------------------------------------------------
    router.post(InfoData.self, at: "inforesponse") { req, data -> InfoResponse in
        return InfoResponse(request: data)
    }
    /* The route handler now returns the new InfoResponse type.
     A new InfoResponse is constructed using the decoded request.
     */
    
    
///---------------------------------------------------------
/// MOVIE
///---------------------------------------------------------
    
///---------------------------------------------------------
/// Return a Movie JSON using URI Parameter
// <base URL>/movies/<id>
///---------------------------------------------------------
    router.get("movies", String.parameter) { req -> Movie in
        do {
            let id = try req.parameters.next(String.self)
            return Movie(id: id,
                         title: "Marvel's Infinity War",
                         year: 2018,
                         thumbnail: "https://cheapuvcodes.com/wp-content/uploads/2018/08/cover-avengersinfinitywar-hd.jpg",
                         genre: ["Action", "Adventure", "Superhero", "Fantasy", "SciFi"],
                         summary: "Iron Man, Thor, the Hulk and the rest of the Avengers unite to battle their most powerful enemy yet -- the evil Thanos. On a mission to collect all six Infinity Stones, Thanos plans to use the artifacts to inflict his twisted will on reality. The fate of the planet and existence itself has never been more uncertain as everything the Avengers have fought for has led up to this moment.",
                         rating: 95
            )
        }
    }
    
///---------------------------------------------------------
/// Accept a Movie JSON
// <base URL>/movies
///---------------------------------------------------------
    router.post(SubmittedMovie.self, at: "movies") { req, data  -> MovieResponse in
        return try MovieResponse(submittedMovie: data)
    }
    
    
    
    
    
///---------------------------------------------------------
/// RW CHALLENGE
///---------------------------------------------------------

///---------------------------------------------------------
/// Return a String data
// <base URL>/data
///---------------------------------------------------------
    router.get("date") { req in
        return "The date today: \(Date())"
    }
    
///---------------------------------------------------------
/// Return the count in JSON
// <base URL>/counter/<number>
///---------------------------------------------------------
    router.get("counter", Int.parameter) { req -> Counter in
        let counter = try req.parameters.next(Int.self)
        return Counter(count: counter)
    }
    
///---------------------------------------------------------
/// Accepts JSON with info & Return a string
// <base URL>/use-info
///---------------------------------------------------------
    router.post(UserInfoData.self, at: "user-info") { req, data -> String in
        let name = data.name
        let age = data.age
        return "Hello, \(name). You are \(age)"
    }
    
    
    
    
    
    let mealController = MealController()
    router.get("meals", use: mealController.index)
    router.post("meals", use: mealController.create)
    /*
     run our program and test our routes.
     To test our routes, it’s best to use a REST client like Postman.
     This will allow us to test GET and POST requests.
     */
}

struct InfoData: Content {
    let name: String
}
/*
 This struct conforms to Content which is Vapor’s wrapper around Codable.
 Vapor uses Content to extract the request data, whether it’s the default JSON-encoded or form URL-encoded.
 InfoData contains the single parameter name.
 */

struct InfoResponse: Content {
    let request: InfoData
}
// This struct conforms to Content and contains a property for the request.


struct Counter: Content {
    let count: Int
}

struct UserInfoData: Content {
    let name: String
    let age: Int
}
