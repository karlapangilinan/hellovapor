//
//  Meal.swift
//  App
//
//  Created by Karla Pangilinan on 04/03/2019.
//

import Foundation
import Vapor
import FluentPostgreSQL

final class Meal: PostgreSQLModel {
    var id: Int?
    var description: String
    
    init(description: String) {
        self.description = description
    }
}

// Allows `Meal` to be used as a dynamic migration.
extension Meal: Migration { }
// Allows `Todo` to be encoded to and decoded from HTTP messages.
extension Meal: Content { }
// Allows `Meal` to be used as a dynamic parameter in route definitions.
extension Meal: Parameter { }

