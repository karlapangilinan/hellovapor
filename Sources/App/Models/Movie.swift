//
//  Movie.swift
//  App
//
//  Created by Karla Pangilinan on 01/03/2019.
//

import Foundation
import Vapor

enum MovieError<T>: Error {
    case invalidParam(_ param: T)
}

struct Movie: Content {
    let id: String
    let title: String
    let year: Int16
    let thumbnail: String
    let genre: [String]
    let summary: String
    let rating: Int8
}

struct SubmittedMovie: Content {
    let title: String
    let year: Int
    let thumbnail: String
    let genre: [String]
    let summary: String
    let rating: Int
}

struct MovieResponse: Content {
    let movie: Movie
    
    init(submittedMovie: SubmittedMovie) throws {
        guard !submittedMovie.title.isEmpty else {
            throw MovieError<String>.invalidParam(submittedMovie.title)
        }
        
        guard submittedMovie.year.digits == 4 else {
            throw MovieError<Int>.invalidParam(submittedMovie.year)
        }

        guard !submittedMovie.thumbnail.isEmpty else {
            throw MovieError<String>.invalidParam(submittedMovie.thumbnail)
        }
        
        guard !submittedMovie.genre.isEmpty else {
            throw MovieError<[String]>.invalidParam(submittedMovie.genre)
        }

        guard !submittedMovie.summary.isEmpty else {
            throw MovieError<String>.invalidParam(submittedMovie.summary)
        }

        guard submittedMovie.rating <= 100 else {
            throw MovieError<Int>.invalidParam(submittedMovie.rating)
        }
        
        
        movie = Movie(id: "BGTHTHD235DFVDFB3461DFB635SVQr",
                      title: submittedMovie.title,
                      year: Int16(submittedMovie.year),
                      thumbnail: submittedMovie.thumbnail,
                      genre: submittedMovie.genre,
                      summary: submittedMovie.summary,
                      rating: Int8(submittedMovie.rating)
        )
    }
}


public extension Int {
    public var digits: Int {
        get {
            return numberOfDigits(in: self)
        }
    }
    // private recursive method for counting digits
    private func numberOfDigits(in number: Int) -> Int {
        if number < 10 && number > 0 || number > -10 && number < 0 {
            return 1
        } else {
            return 1 + numberOfDigits(in: number/10)
        }
    }
}
