//
//  MealController.swift
//  App
//
//  Created by Karla Pangilinan on 04/03/2019.
//

import Foundation
import Vapor
final class MealController {
    // Vapor utilizes the Future type to provide a reference to an object that isn’t available yet
    func index(_ req: Request) throws -> Future<[Meal]> {
        // use one of Fluent’s query methods to return all of the Meal objects stored in our database.
        return Meal.query(on: req).all()
    }
    
    // we decode some JSON that is sent in the request to create a Meal object which we then save.
    func create(_ req: Request) throws -> Future<Meal> {
        return try req.content.decode(Meal.self).flatMap { meal in
            return meal.save(on: req)
        }
    }
}
