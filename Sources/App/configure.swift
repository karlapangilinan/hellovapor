import Vapor
import FluentPostgreSQL

/// Called before your application initializes.
public func configure(_ config: inout Config,
                      _ env: inout Environment,
                      _ services: inout Services) throws {
    // Register the new provider
    try services.register(FluentPostgreSQLProvider())
    // create a PostgreSQLDatabaseConfig and initialize our PostgreSQLDatabase object using our configuration:
    let databaseConfig: PostgreSQLDatabaseConfig
    if let url = Environment.get("DATABASE_URL") {
        databaseConfig = PostgreSQLDatabaseConfig(url: url)!
    } else {
        databaseConfig = PostgreSQLDatabaseConfig(hostname: "localhost",
                                              port: 5432,
                                              username: "Karla",
                                              database: "Karla",
                                              password: nil,
                                              transport: .cleartext)
    }
    let postgres = PostgreSQLDatabase(config: databaseConfig)
    // There are a couple of things to note here.
    // The hostname points to our local computer and 5432 is the default port that postgreSQL runs on.
    // The database string here matches the database we just created.
    
    // add our new postgres database to our DatabaseConfig object:
    var databases = DatabasesConfig()
    databases.add(database: postgres, as: .psql)
    services.register(databases)
    
    
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)
    
    // Create a migration with Fluent & add Meal model
    var migrations = MigrationConfig()
    migrations.add(model: Meal.self, database: .psql)
    services.register(migrations)
    // Run & see that our migrations have run, and our server is running.
}
